//
//  NoteTableViewCell.swift
//  MyNotes
//
//  Created by Роман Хоменко on 01.10.2021.
//

import UIKit

class NoteTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var noteDescriptionLabel: UILabel!
 
    func set(object: Note) {
        self.titleLabel.text = object.title
        self.noteDescriptionLabel.text = object.noteDescription
        self.titleLabel.font = UIFont.systemFont(ofSize: CGFloat(object.fontSize + 10))
        self.noteDescriptionLabel.font = UIFont.systemFont(ofSize: CGFloat(object.fontSize))
    }
}
