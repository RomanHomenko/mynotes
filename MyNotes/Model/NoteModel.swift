//
//  NoteModel.swift
//  MyNotes
//
//  Created by Роман Хоменко on 01.10.2021.
//

import Foundation
import RealmSwift

class Note: Object {
    @objc dynamic var title: String = ""
    @objc dynamic var noteDescription: String = ""
    @objc dynamic var fontSize: Int = 0
    @objc dynamic var image: String = ""
}
