//
//  NoteTableViewController.swift
//  MyNotes
//
//  Created by Роман Хоменко on 01.10.2021.
//

import UIKit
import RealmSwift

class NoteTableViewController: UITableViewController, NoteDelegate {

    private let realm = try! Realm()
    private var objects = [Note]()
    public var deletionhandler: (() -> Void)?
    private var segueObserver: Bool = false
    private var fontSize: Int = 0
    private var catImage: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        startNote()
        objects = realm.objects(Note.self).map { $0 }
        self.title = "MyNotes"
        self.navigationItem.leftBarButtonItem = self.editButtonItem
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return objects.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell", for: indexPath) as! NoteTableViewCell
        let object = objects[indexPath.row]
        cell.set(object: object)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let myNote = objects[indexPath.row]
            
            realm.beginWrite()
            realm.delete(myNote)
            try! realm.commitWrite()
            self.refresh()
            deletionhandler?()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "enter") as? EntryViewController
        vc?.delegate = self
        vc?.segueObserve = true
        vc?.noteTitle = objects[indexPath.row].title
        vc?.noteDescription = objects[indexPath.row].noteDescription
        vc?.noteFont = objects[indexPath.row].fontSize
        vc?.currentIndexPath = indexPath

        vc?.catImage = objects[indexPath.row].image
        self.navigationController?.pushViewController(vc!, animated: true)
        self.refresh()
    }
    
    func startNote() {
        realm.beginWrite()
        
        let newNote = Note()
        newNote.title = "Note"
        newNote.noteDescription = "Description"
        newNote.fontSize = 15
        newNote.image = "cat0"
        realm.add(newNote)
        
        try! realm.commitWrite()
    }
    
    func changeNoteData(noteTitle: String, noteDeskription: String, indexPath: IndexPath) {
        print(indexPath.row)
        try! realm.write {
            objects[indexPath.row].title = noteTitle
            objects[indexPath.row].noteDescription = noteDeskription
            objects[indexPath.row].fontSize = fontSize
            objects[indexPath.row].image = catImage
        }
        self.refresh()
    }
    
    func changeFontSize(size: Int) {
        fontSize = size
        self.refresh()
    }
    
    func saveCatImage(name: String) {
        catImage = name
    }
    
    @IBAction func didTapAddButton() {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "enter") as? EntryViewController else {
            return
        }
        vc.completionHandler = { [weak self] in
            self?.refresh()
        }
        vc.title = "New Note"
        vc.noteFont = 15
        vc.navigationItem.largeTitleDisplayMode = .never
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func refresh() {
        objects = realm.objects(Note.self).map { $0 }
        tableView.reloadData()
    }
    
}

protocol NoteDelegate {
    func changeNoteData(noteTitle: String, noteDeskription: String, indexPath: IndexPath)
    func changeFontSize(size: Int)
    func saveCatImage(name: String)
}
