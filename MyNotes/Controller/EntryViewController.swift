//
//  EntryViewController.swift
//  MyNotes
//
//  Created by Роман Хоменко on 01.10.2021.
//

import UIKit
import RealmSwift

class EntryViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var slider: UISlider! {
        didSet {
            slider.maximumValue = 40
            slider.minimumValue = 1
            slider.value = Float(noteFont)
        }
    }
    @IBOutlet weak var textSizeLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    
    var noteTitle: String = ""
    var noteDescription: String = ""
    var segueObserve: Bool = false
    var currentIndexPath: IndexPath?
    var delegate: NoteDelegate?
    var noteFont: Int = 0
    var catImage: String = ""
    
    private var realm = try! Realm()
    public var completionHandler: (() -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleTextField.becomeFirstResponder()
        descriptionTextField.becomeFirstResponder()
        titleTextField.delegate = self
        descriptionTextField.delegate = self
        titleTextField.text = noteTitle
        descriptionTextField.text = noteDescription
        textSizeLabel.text = String(describing: Int(slider.value))
        imageView.image = UIImage(named: catImage)
        
        titleTextField.font = UIFont.systemFont(ofSize: CGFloat(noteFont))
        descriptionTextField.font = UIFont.systemFont(ofSize: CGFloat(noteFont))
        navigationItem.largeTitleDisplayMode = .never
        if segueObserve == true {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save changes", style: .done, target: self, action: #selector(didTapChangeButton))
        } else {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .done, target: self, action: #selector(didTapSaveButton))
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        titleTextField.resignFirstResponder()
        return true
    }
    
    @objc func didTapSaveButton() {
        if let title = titleTextField.text, !title.isEmpty, let noteDescription = descriptionTextField.text {
            realm.beginWrite()
            
            let newNote = Note()
            newNote.title = title
            newNote.noteDescription = noteDescription
            newNote.fontSize = Int(slider.value)
            newNote.image = catImage
            realm.add(newNote)
            
            try! realm.commitWrite()
            completionHandler?()
            navigationController?.popToRootViewController(animated: true)
        } else {
            let alertController = UIAlertController(title: "Error", message: "Put the title!", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default) { action in
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @objc func didTapChangeButton() {
        if titleTextField.text == "" {
            let alertController = UIAlertController(title: "Error", message: "Put the title!", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .default) { action in
            }
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
        } else {
            delegate?.changeFontSize(size: Int(slider.value))
            delegate?.changeNoteData(noteTitle: titleTextField.text ?? "", noteDeskription: descriptionTextField.text ?? "", indexPath: currentIndexPath!)
            completionHandler?()
            navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func sliderChanged(_ sender: UISlider) {
        let sizeText = Int(sender.value)
        textSizeLabel.text = "\(sizeText)"
        titleTextField.font = UIFont.systemFont(ofSize: CGFloat(sizeText))
        descriptionTextField.font = UIFont.systemFont(ofSize: CGFloat(sizeText))
    }
    
    @IBAction func didTapImageViewButton(_ sender: UIButton) {
        let imageArr = (0...13).compactMap { UIImage(named: "cat\($0)") }
        
        let alertController = UIAlertController(title: "Cat photo", message: "Put number from 0 to 13", preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default) { action in
            if let number = alertController.textFields?.first?.text {
                
                self.imageView.image = imageArr[Int(number) ?? 0]
                self.catImage = "cat\(number)"
                self.delegate?.saveCatImage(name: "cat\(number)")
            }
        }
        alertController.addTextField(configurationHandler: nil)
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
}
